# Tic Tac Toe Game
Simple tic tac toe game, live demo [_here_](https://venerable-sunshine-a0709f.netlify.app).
## Table of Contents

* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Screenshots](#screenshots)

## General Information

## Technologies Used
- React
- Sass
- Axios

## Screenshots
![Example screenshot](./main_view.png)
