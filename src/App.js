import { Route, Routes } from 'react-router';

import Homepage from './components/homepage/homepage.component';

import './App.css';
import GameWithLocation from './components/game/game.component';

function App() {
  return (
    <Routes>
      <Route
        path='/'
        element={<Homepage/>}
      />
      <Route
        path='/game'
        element={<GameWithLocation/>}
      />
    </Routes>
  );
}

export default App;
