const axios = require('axios');
class APIObject {
  #requestQueue;

  constructor () {
    this.#requestQueue = [];
  }

  #wait5sec(waitTime) {
    return new Promise ((resolve) => {
      setTimeout(() => {
        resolve(true);
      }, waitTime);
    });
  };

  async getCharacteresData(listCharacterId) {
    this.#requestQueue = listCharacterId.map(({playerId}) => playerId);
    let data = [];

    while(this.#requestQueue.length > 0) {
      let playerId = this.#requestQueue.pop();
      await this.#wait5sec(2000);
      
      data.push(await new Promise((resolve, reject) => {
        axios.get(`${JIKAN_BASE_URL}/characters/${playerId}`)
        .then(response => {
          const { data } = response.data;
  
          resolve({
            id: data.mal_id,
            name: data.name,
            imageUrl: data.images.jpg.image_url
          });
        })
        .catch(error => {
          console.log(error);
        });
      }));
    }

    return data;
  }
};

export const JikanAPI = new APIObject();
export const ANIME_ID = 10165;
export const JIKAN_BASE_URL = 'https://api.jikan.moe/v4';