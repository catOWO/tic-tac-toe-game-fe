import { Component } from "react";
import io from "socket.io-client";
import axios from "axios";
import queryString from 'query-string';
import { useLocation } from "react-router";

import Modal from "../modal/modal.component";
import Board from "../board/board.component";
// import SideBar from "../sidebar/sidebar.component";

import './game.styles.scss';

import { JIKAN_BASE_URL } from "../../api/api-config";
// import { JikanAPI } from "../../api/api-config";

// const ENDPOINT = 'http://localhost:5000/';
const ENDPOINT = 'https://tic-tac-toe-game-be.herokuapp.com/';
const DRAW_GAME = 'draw';

const GameWithLocation = props => {
  const location = useLocation()

  return <Game location={location} {...props} />
}

class Game extends Component {
  constructor(props) {
    super();
    const { playerId } = this.getQueryParams(props);
    this.state = {
      board: Array(9).fill(null),
      playerIdTurn: playerId,
      playerTopId: playerId,
      playerTop: null,
      winner: null,
      showModal: false,
      playerBottom: null,
      playersData: []
    };
  }

  getQueryParams({ location }) {
    return queryString.parse(location.search);
  }

  loadIATimerDelay() {
    return new Promise(resolve => setTimeout(() => resolve(), 1500));
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async fetchTopPlayerData(playerId) {
    return new Promise((resolve, reject) => {
      axios.get(`${JIKAN_BASE_URL}/characters/${playerId}`)
      .then(response => {
        const { data } = response.data;

        resolve({
          id: data.mal_id,
          name: data.name,
          imageUrl: data.images.jpg.image_url
        });
      })
      .catch(error => {
        console.log(error);
      });
    });
  }

  async componentDidMount() {
    const { location } = this.props;
    const { playerId, iaPlayerId } = queryString.parse(location.search);

    this.socket = io(ENDPOINT);
    this.socket.emit('joinGame', {
      roomId: 1,
      playerId
    });

    this.socket.on('humanPlayed', ({ board, winner }) => {
      console.log('Human played ', board, ' ...', winner);
      this.setState({
        board,
        winner
      });

      if (winner === null || winner !== DRAW_GAME) {
        this.socket.emit('iaPlaying', { roomId: 1 });
        this.setState({
          playerIdTurn: -1
        });
      }

      if (winner) {
        this.setState({
          showModal: true,
          playerIdTurn: null
        });
      }
    });

    this.socket.on('iaPlayed', ({ board, winner }) => {
      console.log('IA played', board, winner);

      this.loadIATimerDelay().then(() => {
        this.setState({
          board,
          winner
        });

        if (winner === null || winner !== DRAW_GAME) {
          const { playerTopId } = this.state;
          this.setState({
            playerIdTurn: playerTopId
          });
        }

        if (winner) {
          this.setState({
            showModal: true,
            playerIdTurn: null
          });
        }
      });
    });

    // this.socket.on('newPlayers', async newPlayers => {
    //   const newPlayersData = await JikanAPI.getCharacteresData(newPlayers);
    //   this.setState({
    //     playersData: newPlayersData
    //   });
    // });

    const playerData = await this.fetchTopPlayerData(playerId);
    await this.timeout(2000);
    const playerIAData = await this.fetchTopPlayerData(iaPlayerId);
    
    if (playerData) {
      this.setState({
        playerTop: playerData,
        playerBottom: playerIAData
      });
    }
  }

  componentWillUnmount() {
    this.socket.disconnect();
  }

  handleClickSquare = ({ target }) => {
    const squareId = parseInt(target.id, 10);

    const { playerTopId, playerIdTurn } = this.state;

    if (playerTopId === playerIdTurn) {
      this.socket.emit('humanPlayerClick', {
        squareId,
        roomId: 1
      });
    }
  }

  handleCloseModal = () => {
    this.setState({
      showModal: false
    });
  }

  render() {
    const {
      board,
      winner,
      showModal,
      playerTop,
      playerIdTurn,
      playerBottom,
      playersData
    } = this.state;

    const ModalMessage = winner === DRAW_GAME ? "It's a draw" : (
      winner === 'O' ? 'You Win!' : 'Bot Win!'
    );

    return (
      <div className="game-layout">
        {
          showModal && 
          <Modal closeAction={this.handleCloseModal} title={''}>
            <p>
              { ModalMessage }
            </p>
          </Modal>
        }
        <Board 
          board={board}
          clickSquare={this.handleClickSquare}
          playerTop={playerTop}
          playerIdTurn={playerIdTurn}
          playerBottom={playerBottom}
        />
        <div className="board-layout-sidebar">
          {/* <SideBar players={playersData}/> */}
        </div>
      </div>
    );
  }
}

export default GameWithLocation;