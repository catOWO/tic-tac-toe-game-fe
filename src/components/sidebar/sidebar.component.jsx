import React from "react";
import { Tab, Tabs } from "react-bootstrap";
import PlayerItemList from "../playerItemList/playeritemlist.component";

import './sidebar.styles.scss';

const SideBar = ({ players }) => (
  <div className="sidebar">
    <Tabs defaultActiveKey="players" id="uncontrolled-tab-example" className="mb-3">
      <Tab
        eventKey="players"
        title={
          <div className="custom-tab-content">
            <span>
              <i className="fa-solid fa-users fa-2xl"></i>
            </span>
            <span>
              Players
            </span>
          </div>
      }>
        <div>
          {
            players.map(
              ({id, name, imageUrl}) => 
                <PlayerItemList
                  id={id}
                  key={id}
                  name={name}
                  imageUrl={imageUrl}
                />
            )
          }
        </div>
      </Tab>
    </Tabs>
  </div>
);

export default SideBar;