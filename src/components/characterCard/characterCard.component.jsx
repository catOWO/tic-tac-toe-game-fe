import React from "react";

import './characterCard.styles.scss';

const CharacterCard = ({ id, name, imageUrl, clickAction }) => (
  <div className="characterCard" id={id} onClick={clickAction}>
    <img id={id} src={imageUrl} alt="character-img"/>
    <p id={id}>{name}</p>
  </div>
);

export default CharacterCard;