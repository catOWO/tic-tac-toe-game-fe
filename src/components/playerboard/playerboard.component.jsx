import React from "react";
import CountDownTimer from "../countdowntimer/countdowntimer";

import './playerboard.styles.scss';

const PlayerBoard = ({ id, name, imageUrl, showTimer }) => (
  <div className="player-board-layout">
    <div id={id} className="player-board">
      <div className="player-avatar">
        <img src={imageUrl} alt="player-avatar"/>
      </div>
      <div className="player-tag-name">
        <p>{name}</p>
      </div>
    </div>
    {
      showTimer &&
      <div className="countdown-container">
        <CountDownTimer/>
      </div>
    }
  </div>
);

export default PlayerBoard;