
import { Component } from "react";
import { Navigate } from "react-router-dom";
import axios from "axios";

import { ANIME_ID, JIKAN_BASE_URL } from "../../api/api-config";
import CharacterCard from "../characterCard/characterCard.component";
import './homepage.styles.scss';

// const Homepage = () => (
//   <div className="homepage-body">
//     <div>

//     </div>
//     <p>Homi</p>
//     <Link to={`/game?room=1`}>
//       <button className="t-button" type="submit">Play</button>
//     </Link>
//   </div>
// );

class Homepage extends Component {
  constructor() {
    super();
    this.state = {
      showPlayMode: true,
      characters: [],
      queryRoom: null,
      goRoom: false,
      playWithIA: false
    };
  }

  async fetchMainCharacters() {
    return await new Promise((resolve, reject) => {
      axios.get(`${JIKAN_BASE_URL}/anime/${ANIME_ID}/characters`)
        .then(response => {
          const { data } = response;
          resolve(data.data.filter(character => character.role === 'Main'));
        })
        .catch(error => {
          console.error(error);
        });
    });
  }

  async componentDidMount() {
    const { characters } = this.state;
    if (!characters.length) {
      const charactersData = (await this.fetchMainCharacters()).map((characterData) => {
        let newCharacterData = {};

        newCharacterData.id = characterData.character.mal_id;
        newCharacterData.name = characterData.character.name;
        newCharacterData.imageUrl = characterData.character.images.jpg.image_url;

        return newCharacterData;
      });

      this.setState({
        characters: charactersData
      });
    }
  }

  handlePlayOnline = () => {
    this.setState({
      showPlayMode: false,
      playWithIA: true
    });
  }

  handleChoosePlayer = ({ target }) => {
    const { playWithIA, characters } = this.state;
    let iaPlayerId = null;

    if (playWithIA === true) {
      const remainCharacteres = characters.slice().filter(character => character.id !== parseInt(target.id, 10));
      iaPlayerId = remainCharacteres[Math.floor(Math.random() * remainCharacteres.length)].id;
    }

    this.setState({
      queryRoom: `/game?playerId=${target.id}&room=id${Math.random().toString(16).slice(2)}&playerTop=true&iaPlayerId=${iaPlayerId}`,
      goRoom: true
    });
  }

  render() {
    const {
      showPlayMode,
      characters,
      goRoom,
      queryRoom
    } = this.state;

    return (
      <div className="homepage-main">
        <div className="homepage-title">
          <p>TicTacToe Game</p>
        </div>
        <div className="homepage-body">
          {
            showPlayMode &&
            <div className="play-mode">
              <button className="play-online-button" onClick={this.handlePlayOnline}>
                Play Online
              </button>
            </div>
          }
          {goRoom === true && <Navigate to={queryRoom} replace={true}/>}
          {
            showPlayMode === false &&
            <div className="available-characteres-container">
              <p className="select-player">Select a player</p>
              <div className="available-characters-table">
                { characters.map(({id, name, imageUrl}) => 
                    <CharacterCard
                      key={id}
                      id={id}
                      name={name}
                      imageUrl={imageUrl}
                      clickAction={this.handleChoosePlayer}
                    />
                  )
                }
              </div>
            </div>
          }
        </div>
      </div>
    );
  }
} 

export default Homepage;