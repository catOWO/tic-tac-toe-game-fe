import React from "react";
import { RiCloseLine } from 'react-icons/ri';

import styles from './modal.module.css';

const Modal = ({ closeAction, children }) => {
  return (
    <>
      <div className={styles.darkBG} onClick={closeAction} />
      <div className={styles.centered}>
        <div className={styles.modal}>
          <div className={styles.modalHeader}>
            <h5 className={styles.heading}>{}</h5>
          </div>
          <button
            className={styles.closeBtn}
            onClick={closeAction}
          >
            <RiCloseLine style={{ marginBottom: "-3px" }} />
          </button>
          <div className={styles.modalContent}>
            { children }
          </div>
          <div className={styles.modalActions}>
            <div className={styles.actionsContainer}>
              {/* <button
                className={styles.deleteBtn}
                onClick={() => setIsOpen(false)}
              >
                Delete
              </button> */}
              <button
                className={styles.cancelBtn}
                onClick={closeAction}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Modal;