import React from "react";

const PlayerItemList = ({ id, name, imageUrl }) => (
  <div className="player-item-list">
    <img src={imageUrl} alt="player-item-list"/>
  </div>
);

export default PlayerItemList;