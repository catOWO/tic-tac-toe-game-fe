import { Component } from "react";

import './countdowntimer.scss';

class CountDownTimer extends Component {
  constructor({ stopAction }) {
    super();
    this.stopAction = stopAction;
    this.state = {
      seconds: 10
    };
  }

  componentDidMount() {
    this.timer = setInterval(() => {
      const { seconds } = this.state;
      if (seconds > 0) {
        this.setState(({ seconds }) => ({
          seconds: seconds - 1
        }));
      }
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    const { seconds } = this.state;
    return (
      <div className="timer">
        <p> {seconds} </p>
      </div>
    );
  }
};

export default CountDownTimer;