import React from "react";
import PlayerBoard from "../playerboard/playerboard.component";
import Square from "../square/square.component";

import './board.styles.scss';

const Board = ({ board, clickSquare, playerTop, playerIdTurn, playerBottom }) => (
  <div className="board-layout">
    { playerTop &&
      <PlayerBoard
        id={playerTop.id}
        name={`${playerTop.name} (You)`}
        imageUrl={playerTop.imageUrl}
        showTimer={playerTop.id === parseInt(playerIdTurn, 10)}
      />
    }
    <div className={`tic-tac-toe-board ${playerTop && playerTop.id !== parseInt(playerIdTurn, 10) ? "disable-board" : ""}`}>
      {
        board.map((mark, index) =>
          <Square
            key={index}
            mark={mark}
            id={index}
            clickSquare={clickSquare}
            blockDiv={((playerTop && (playerTop.id !== parseInt(playerIdTurn, 10))) || (mark !== null))}
          />
        )
      }
    </div>
    {
      playerBottom &&
      <PlayerBoard
        id={playerBottom.id}
        name={`${playerBottom.name} (Bot)`}
        imageUrl={playerBottom.imageUrl}
        showTimer={false}
      />
    }
  </div>
);

export default Board;