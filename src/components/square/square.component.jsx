import React from "react";

import './square.styles.scss';

import oMark from '../../assets/o-mark.png';
import xMark from '../../assets/x-mark.png';

const IMAGE_MAP = {
  O: oMark,
  X: xMark
};

const Square = ({ mark, clickSquare, id, blockDiv }) => (
  <div id={id} className="board-square" onClick={blockDiv === true ? null : clickSquare}>
    { mark && mark === 'X' && <img src={IMAGE_MAP[mark]} alt="x-square"></img>}
    { mark && mark === 'O' && <img src={IMAGE_MAP[mark]} alt="o-square"></img>}
  </div>
);

export default Square;